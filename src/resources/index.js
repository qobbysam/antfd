export default [

  {
    name: "posts",
    icon: "mdi-post",
  },
  {
    name: "albums",
    icon: "mdi-post",
  },

  
  
  {
    name: "photos",
    icon: "mdi-photo",
  },
  {
    name: "users",
    icon: "mdi-account",
    routes: ["list"],
  },

  {
    name: "playersimples",
    icon: "mdi-account",
    routes: ["player"],
  },



  {
    name: "reviews",
    icon: "mdi-comment",
    permissions: ["admin", "editor", "author"],
  },
];
