import Vue from "vue";
import Vuex from "vuex";

import oneplayer from "./oneplayer/oneplayer.js";
import VueNativeSock from "vue-native-websocket";
Vue.use(Vuex);

Vue.use(VueNativeSock, "ws://127.0.0.1:7000/ws", { store:  new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {oneplayer,},
})});

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    oneplayer,
  },
});
