/* eslint-disable no-unused-vars */
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const state = {
  cmd: [
    {
      Pwserver: "Item 1",
      MoveName: "Item 2",
      Action: "Item q",
      ActionValue: "vsvd",
    },
  ],
  socket: {
    isConnected: false,
    message: "",
    reconnectError: false,
  },
};

const getters = {
  allCmd: (state) => state.cmd,
};

const actions = {
  addMsg({ commit }, msg) {
    console.log(msg);
    commit("newMsg", msg);
  },
  sendMsg({ commit }, message) {
    Vue.prototype.$socket.send(JSON.stringify(message));
  },
};

const mutations = {
  SOCKET_ONOPEN(state, event) {
    Vue.prototype.$socket = event.currentTarget;
    state.socket.isConnected = true;
    console.log("connected !!");
  },
  SOCKET_ONCLOSE(state, event) {
    event.currentTarget;
    state.socket.isConnected = false;
  },
  SOCKET_ONERROR(state, event) {
    console.error(state, event);
  },
  // default handler called for all methods
  SOCKET_ONMESSAGE(state, message) {
    state.socket.message = message;
  },
  // mutations for reconnect methods
  SOCKET_RECONNECT(state, count) {
    console.info(state, count);
  },
  SOCKET_RECONNECT_ERROR(state) {
    state.socket.reconnectError = true;
  },
  newMsg: (state, msg) => state.cmd.unshift(msg),
};

export default {
  state,
  getters,
  actions,
  mutations,
};
