/* eslint-disable no-unused-vars */

export default (i18n, admin) => [
  {
    icon: "mdi-view-dashboard",
    text: i18n.t("menu.dashboard"),
    link: "/",
  },

  {
    icon: "mdi-view-dashboard",
    text :"Some Dashboard",
    link: "/about"
  },
  { divider: true },

  admin.getResourceLink("users"),
  admin.getResourceLink("posts"),
  admin.getResourceLink("photos"),
  admin.getResourceLink("albums"),
 // { divider:true }, 
 // admin.getResourceLink("playersimple"),
];
